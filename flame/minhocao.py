import os
import sys
import shutil
import re
import time
import math
import json
import yaml
from yaml import Loader 
from collections import defaultdict

from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar
from pymatgen.core.periodic_table import Element

from structure.io_db import get_element_list
from structure.core import min_distance
from config import *

import flame.flame_functions.atoms
from flame.flame_functions.ascii import *
from flame.flame_functions.io_yaml import *
from flame.flame_functions.vasp import *
from flame.flame_functions.latvec2dproj import *


def write_minhocao_files(struct, step_number):
#element list
    elmnt_list = get_element_list()
#
    minhocao_temp = input_list['minhocao_temp']
    minhocao_max_temp = input_list['minhocao_max_temp']
    minhocao_n_min = input_list['minhocao_n_min']
    number_of_epoch = input_list['number_of_epoch']

#write poscur for minhocao both vasp and ascii format
    Poscar(struct).write_file('poscur.vasp')
    atoms=poscar_read('poscur.vasp')
    atoms.cellvec,atoms.rat=latvec2dproj(atoms.cellvec,atoms.rat,atoms.nat)
    ascii_write(atoms, 'poscur.ascii')

    site_symbols = []
    for site in struct:
        site_symbols.append(site.specie.symbol)
#write flame_in.yaml for minhocao
    with open('flame_in.yaml', 'a') as f:
        f.write('main:'+'\n')
        f.write('    task: minhocao'+'\n')
        f.write('    verbosity: 0'+'\n')
        f.write('    types:')
        for elmnt in elmnt_list:
            f.write(' {}'.format(elmnt))
        f.write('\n')
        f.write('    nat: {}'.format(len(struct.sites))+'\n')
        f.write('    typat:')
        for i in range(len(elmnt_list)):
            f.write(' {}*{} '.format(site_symbols.count(elmnt_list[i]),i+1))
        f.write('\n')
        f.write('    pressure: 0.0'+'\n')
        f.write('    verbose: 2'+'\n')
        f.write('    znucl: [')
        for e in range(len(elmnt_list)-1):
            f.write('{}, '.format(Element(elmnt_list[e]).Z))
        f.write('{}'.format(Element(elmnt_list[-1]).Z))
        f.write(']\n')
        f.write('    amass: [')
        for e in range(len(elmnt_list)-1):
            eam = str(Element(elmnt_list[e]).atomic_mass)
            f.write('{}, '.format(re.sub(' amu','',eam)))
        eam = str(Element(elmnt_list[-1]).atomic_mass)
        f.write('{}'.format(re.sub(' amu','',eam)))
        f.write(']\n')
        f.write('    findsym: True'+'\n'+'\n')
        f.write('potential:'+'\n')
        f.write('    potential: ann'+'\n')
        f.write('    core_rep: True'+'\n'+'\n')

        f.write('ann:'+'\n')
        f.write('    approach: atombased'+'\n'+'\n')

        f.write('geopt:'+'\n')
        f.write('    nit: 1900'+'\n')
        f.write('    #geoext: False'+'\n')
        f.write('    method: FIRE'+'\n')
        f.write('    fmaxtol: 1.E-4'+'\n')
        f.write('    strfact: 100.0'+'\n')
        f.write('    dt_start: 5.0'+'\n')
        f.write('    dt_min: 1.0'+'\n')
        f.write('    dt_max: 140.0'+'\n'+'\n')

        f.write('dynamics:'+'\n')
        f.write('    nmd: 1000'+'\n')
        f.write('    cellmass: 2.0'+'\n')
        f.write('    dt_init: 20.0'+'\n')
        f.write('    auto_mdmin: True'+'\n')
        f.write('    auto_mddt: True'+'\n')
        f.write('    nit_per_min: 30'+'\n')
        f.write('    mdmin_min: 2'+'\n')
        f.write('    mdmin_max: 4'+'\n'+'\n')

        f.write('minhopp:'+'\n')
        f.write('    auto_soft: True'+'\n')
        f.write('    nsoften: 20'+'\n')
        f.write('    alpha_at: 1.0'+'\n')
        f.write('    alpha_lat: 1.0'+'\n'+'\n')

        f.write('fingerprint:'+'\n')
        f.write('    method: OGANOV'+'\n')
        f.write('    rcut: 15.0'+'\n')
        f.write('    #dbin: 0.06'+'\n')
        f.write('    #sigma: 0.04'+'\n')
#write ioput for minhocao
    s_n = int(re.split('-',step_number)[1]) - 1
    with open('ioput', 'w') as f:
       f.write('  0.01        {}       {}         ediff, temperature, maximal temperature'.format(minhocao_temp[s_n], minhocao_max_temp[s_n])+'\n')
#write earr.dat for minhocao
    with open('earr.dat', 'w') as f:
        f.write('  0         {}          # No. of minima already found, no. of minima to be found in consecutive run'.format(minhocao_n_min[s_n])+'\n')
        f.write('  0.400000E-03  0.150000E+00  # delta_enthalpy, delta_fingerprint')
#cp ann files from train directory
    number_of_epoch = input_list['number_of_epoch']
    for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'train',)):
        for f in files:
            for elmnt in elmnt_list:
                if f == str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5):
                    shutil.copyfile(os.path.join(root,str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)),\
                                    os.path.join('./',str(elmnt)+'.ann.param.yaml'))


def store_minhocao_results(step_number):
#a dynamicd allowed minimum distance
    min_d = min_distance() * (1.1-(int(re.split('-',step_number)[1])/10))
    if (1.1-(int(re.split('-',step_number)[1])/10)) < 0.5:
        min_d = min_distance() * 0.5
    nat_list = []
    all_minhocao_structs = defaultdict(list) 
# collect data from minhocao folder
# poslow files
    for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'minhocao')):
        if 'poscur.ascii' in files:
            if 'global.mon' in files:
                ref_struct = Structure.from_file(os.path.join(root,'poscur.vasp'))
                n_atom = len(ref_struct.sites)
                if n_atom not in nat_list:
                    all_minhocao_structs[n_atom] = []
                    nat_list.append(n_atom)
                for a_file in files:
                    if 'poslow' in a_file and a_file.endswith('ascii'):
                        atoms=ascii_read(os.path.join(root,a_file))
                        atoms.units_length_io='angstrom'
                        conf=atoms2dict(atoms)
                        lattice = conf['conf']['cell']
                        crdnts = []
                        spcs = []
                        for coord in conf['conf']['coord']:
                            crdnts.append([coord[0],coord[1],coord[2]])
                            spcs.append(coord[3])
                        structure=Structure(lattice,spcs,crdnts,coords_are_cartesian=True)
                        if structure.is_valid(min_d) and structure.lattice.a < ref_struct.lattice.a * 2\
                                                     and structure.lattice.b < ref_struct.lattice.b * 2\
                                                     and structure.lattice.c < ref_struct.lattice.c * 2:
                            all_minhocao_structs[n_atom].append(conf)
            else:
                with open(log_file, 'a') as f:
                    f.write('no global.mon file is found in {}'.format(root)+'\n')

# T folders
    for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'minhocao')):
        T_folder_paths = []
#
        if 'poscur.vasp' in files:
            if 'global.mon' in files:
                ref_struct = Structure.from_file(os.path.join(root,'poscur.vasp'))
                n_atom = len(ref_struct.sites)
                if len(all_minhocao_structs[n_atom]) > 9999:
                    continue
                with open(os.path.join(root,'global.mon'), 'r') as f:
                    lines = f.readlines()[1:]
                for l in lines:
                    f_i = str(re.split('\s+',l)[1]).zfill(5)
                    if re.split('\s+',l)[11] == 'T':
                        T_folder_paths.append(os.path.join(root,'data_hop_'+f_i))
                for tfolderpath in T_folder_paths:
                    if len(all_minhocao_structs[n_atom]) > 9999:
                        break
                    tposmd_file_list = []
                    for tfiles in os.listdir(tfolderpath):
                        if 'posmd' in tfiles and tfiles.endswith('ascii'):
                            tposmd_file_list.append(tfiles)
                    for t in range(0,len(tposmd_file_list),20):
                        if len(all_minhocao_structs[n_atom]) > 9999:
                            break
                        atoms=ascii_read(os.path.join(tfolderpath,tposmd_file_list[t]))
                        atoms.units_length_io='angstrom'
                        conf=atoms2dict(atoms)
                        lattice = conf['conf']['cell']
                        crdnts = []
                        spcs = []
                        for coord in conf['conf']['coord']:
                            crdnts.append([coord[0],coord[1],coord[2]])
                            spcs.append(coord[3])
                        structure=Structure(lattice, spcs, crdnts, coords_are_cartesian=True)
                        if structure.is_valid(min_d) and structure.lattice.a < ref_struct.lattice.a * 2\
                                                     and structure.lattice.b < ref_struct.lattice.b * 2\
                                                     and structure.lattice.c < ref_struct.lattice.c * 2:
                            all_minhocao_structs[n_atom].append(conf)
#F folders
    for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'minhocao')):
        F_folder_paths = []
#
        if 'poscur.vasp' in files:
            if 'global.mon' in files:
                ref_struct = Structure.from_file(os.path.join(root,'poscur.vasp'))
                n_atom = len(ref_struct.sites)
                if len(all_minhocao_structs[n_atom]) > 9999:
                    continue
                with open(os.path.join(root,'global.mon'), 'r') as f:
                    lines = f.readlines()[1:]
                for l in lines:
                    f_i = str(re.split('\s+',l)[1]).zfill(5)
                    if re.split('\s+',l)[11] == 'F':
                        F_folder_paths.append(os.path.join(root,'data_hop_'+f_i))
                for ffolderpath in F_folder_paths:
                    if len(all_minhocao_structs[n_atom]) > 9999:
                        break
                    fposmd_file_list = []
                    for ffiles in os.listdir(ffolderpath):
                        if 'posmd' in ffiles and ffiles.endswith('ascii'):
                            fposmd_file_list.append(ffiles)
                        for f in range(0,len(fposmd_file_list),20):
                            if len(all_minhocao_structs[n_atom]) > 9999:
                                break
                            atoms=ascii_read(os.path.join(ffolderpath,fposmd_file_list[f]))
                            atoms.units_length_io='angstrom'
                            conf=atoms2dict(atoms)
                            lattice = conf['conf']['cell']
                            crdnts = []
                            spcs = []
                            for coord in conf['conf']['coord']:
                                crdnts.append([coord[0],coord[1],coord[2]])
                                spcs.append(coord[3])
                            structure=Structure(lattice, spcs, crdnts, coords_are_cartesian=True)
                            if structure.is_valid(min_d) and structure.lattice.a < ref_struct.lattice.a * 2\
                                                         and structure.lattice.b < ref_struct.lattice.b * 2\
                                                         and structure.lattice.c < ref_struct.lattice.c * 2:
                                all_minhocao_structs[n_atom].append(conf)
#S folders
    for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'minhocao')):
        S_folder_paths = []
#      
        if 'poscur.vasp' in files:
            if 'global.mon' in files:
                ref_struct = Structure.from_file(os.path.join(root,'poscur.vasp'))
                n_atom = len(ref_struct.sites)
                if len(all_minhocao_structs[n_atom]) > 9999:
                    continue
                with open(os.path.join(root,'global.mon'), 'r') as f:
                    lines = f.readlines()[1:]
                for l in lines:
                    f_i = str(re.split('\s+',l)[1]).zfill(5)
                    if re.split('\s+',l)[11] == 'S':
                        S_folder_paths.append(os.path.join(root,'data_hop_'+f_i))
                for sfolderpath in S_folder_paths:
                    if len(all_minhocao_structs[n_atom]) > 9999:
                        break
                    sposmd_file_list = []
                    for sfiles in os.listdir(sfolderpath):
                        if 'posmd' in sfiles and sfiles.endswith('ascii'):
                            sposmd_file_list.append(sfiles)
                        for s in range(0,len(sposmd_file_list),20):
                            if len(all_minhocao_structs[n_atom]) > 9999:
                                break
                            atoms=ascii_read(os.path.join(sfolderpath,sposmd_file_list[s]))
                            atoms.units_length_io='angstrom'
                            conf=atoms2dict(atoms)
                            lattice = conf['conf']['cell']
                            crdnts = []
                            spcs = []
                            for coord in conf['conf']['coord']:
                                crdnts.append([coord[0],coord[1],coord[2]])
                                spcs.append(coord[3])
                            structure=Structure(lattice, spcs, crdnts, coords_are_cartesian=True)
                            if structure.is_valid(min_d) and structure.lattice.a < ref_struct.lattice.a * 2\
                                                         and structure.lattice.b < ref_struct.lattice.b * 2\
                                                         and structure.lattice.c < ref_struct.lattice.c * 2:
                                all_minhocao_structs[n_atom].append(conf)

    with open(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json'), 'w') as f:
        json.dump(all_minhocao_structs, f)
    
