import os
import yaml
import json
import re
from config import *
import stat
import math
from random import sample

from pymatgen.core.structure import Structure 
from structure.io_db import get_element_list
from flame.core import *

def write_train_files(step_number):
    training_set = [] 
    di = {}
    s_n = int(re.split('-',step_number)[1])
    p_s_path  = os.path.join(Flame_dir,'step-'+str(s_n-1))
# add data from bulk optimization
    if s_n == 1:
        for root, dirs, files in os.walk(bulk_structure_optimization_dir):
            if 'task.json' in files:
                with open(os.path.join(root,'task.json'), 'r') as f:
                    s = json.loads(f.read())
#converged force,structure, and energy
                di['structure'] = s['output']['structure']
                di['forces'] = s['output']['forces']
                di['energy'] = s['output']['energy']
                training_set.append(di)
                di = {}
#select the iteration with differenet total forces
                found_1 = False
                found_2 = False
                found_3 = False
                found_4 = False
                found_5 = False
                for i in range(1,len(s['calcs_reversed'][0]['output']['ionic_steps'])):
                    this_forces = s['calcs_reversed'][0]['output']['ionic_steps'][i]['forces']
                    this_tot_forces = []
                    for j in range(0,len(this_forces)):
                        this_tot_forces.append(math.sqrt(this_forces[j][0]**2 + this_forces[j][1]**2 + this_forces[j][2]**2))
                    max_this_tot_foce = max(this_tot_forces)

                    if max_this_tot_foce < 3.0 and max_this_tot_foce > 2.5 and not found_1:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_1 = True
                    if max_this_tot_foce < 2.5 and max_this_tot_foce > 2.0 and not found_2:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_2 = True
                    if max_this_tot_foce < 2.0 and max_this_tot_foce > 1.5 and not found_3:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_3 = True
                    if max_this_tot_foce < 1.5 and max_this_tot_foce > 1.0 and not found_4:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_4 = True
                    if max_this_tot_foce < 1.0 and max_this_tot_foce > 0.5 and not found_5:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_5 = True
#read traning setfrom the previous step
    if s_n > 1:
        for root, dirs, files in os.walk(os.path.join(p_s_path,'train')):
            if 'position_force_train_all.json' in files:
                with open(os.path.join(root,'position_force_train_all.json'), 'r') as f:
                    f_s=json.loads(f.read())
                training_set.extend(f_s)
#add data from the previuos step task files
    for root, dirs, files in os.walk(os.path.join(p_s_path,'task_files')):
        for f in files:
            if f.split(".")[-1] == 'json':
                with open(os.path.join(root,f), 'r') as f:
                    s = json.loads(f.read())
#converged force,structure, and energy
                di['structure'] = s['output']['structure']
                di['forces'] = s['output']['forces']
                di['energy'] = s['output']['energy']
                training_set.append(di)
                di = {}
#select the iteration with differenet total forces
                found_1 = False
                found_2 = False
                found_3 = False
                found_4 = False
                found_5 = False
#
                for i in range(1,len(s['calcs_reversed'][0]['output']['ionic_steps'])):
                    this_forces = s['calcs_reversed'][0]['output']['ionic_steps'][i]['forces']
                    this_tot_forces = []
                    for j in range(0,len(this_forces)):
                        this_tot_forces.append(math.sqrt(this_forces[j][0]**2 + this_forces[j][1]**2 + this_forces[j][2]**2))
                    max_this_tot_foce = max(this_tot_forces)
#
                    if max_this_tot_foce < 3.0 and max_this_tot_foce > 2.5 and not found_1:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces   
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_1 = True
                    if max_this_tot_foce < 2.5 and max_this_tot_foce > 2.0 and not found_2:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_2 = True
                    if max_this_tot_foce < 2.0 and max_this_tot_foce > 1.5 and not found_3:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_3 = True
                    if max_this_tot_foce < 1.5 and max_this_tot_foce > 1.0 and not found_4:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_4 = True
                    if max_this_tot_foce < 1.0 and max_this_tot_foce > 0.5 and not found_5:
                        di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['structure']
                        di['forces'] = this_forces
                        di['energy'] = s['calcs_reversed'][0]['output']['ionic_steps'][i]['e_wo_entrp']
                        training_set.append(di)
                        di = {}
                        found_5 = True

    indices_list = list(range(len(training_set)))
#write all data
    json_dump = []
    for i in indices_list:
        json_dump.append(training_set[i])
    with open('position_force_train_all.json', 'w') as f:
        json.dump(json_dump, f)

#    f_name = 'position_force_train_all.yaml'
#    for i in indices_list:
#        write_p_f_from_Structure(Structure.from_dict(training_set[i]['structure']),training_set[i]['energy'],training_set[i]['forces'],f_name)

#indices for validation 
    valid_indices = sample(indices_list, int(len(indices_list)/10))
    for rem in valid_indices:
        indices_list.remove(rem)
#indices for training
    train_indices = indices_list

#write valid data 
    f_name ='position_force_train_valid.yaml'
    for i in valid_indices:
        write_p_f_from_Structure(Structure.from_dict(training_set[i]['structure']),training_set[i]['energy'],training_set[i]['forces'],f_name)
#write list_posinp_check.yaml for valid
    with open('list_posinp_valid.yaml', 'a') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_train_valid.yaml'+'\n')
#write list_posinp_check.yaml for train
    with open('list_posinp_train.yaml', 'a') as f:
        f.write('files:'+'\n')
#write all train data
    t = int(len(indices_list)/10000)
    samp = int(len(indices_list)/(t+1))
    for i in range(1, t+2):
        f_name = 'position_force_train_train_'+'t'+str(i).zfill(3)+'.yaml'
        train_indices_t = sample(train_indices,samp)
        for rem in train_indices_t:
            write_p_f_from_Structure(Structure.from_dict(training_set[rem]['structure']),training_set[rem]['energy'],training_set[rem]['forces'],f_name)
            train_indices.remove(rem)
#write list_posinp_check.yaml for train
        with open('list_posinp_train.yaml', 'a') as f:
            f.write(' - {}'.format('position_force_train_train_'+'t'+str(i).zfill(3)+'.yaml'+'\n'))
#write SE.ann.input.yaml
    elmnt_list = get_element_list()
    write_SE_ann_FLAME(elmnt_list)
#write flame_in.yaml for train
    with open('flame_in.yaml', 'w') as f:
        f.write('main:'+'\n')
        f.write('    task: ann'+'\n')
        f.write('    seed: 234511'+'\n')
        f.write('    types:')
        for elmnt in elmnt_list:
             f.write(' {}'.format(elmnt))
        f.write('\n')
        f.write('    verbosity: 1')
        f.write('\n'+'\n')

        f.write('ann:'+'\n')
        f.write('    subtask: train'+'\n')
        f.write('    optimizer: rivals'+'\n')
        f.write('    approach: atombased'+'\n')
        f.write('    nstep_opt: {}'.format(input_list['number_of_epoch'])+'\n')
        f.write('    nconf_rmse: 500'+'\n')
        f.write('    ampl_rand: 0.005'+'\n')
        f.write('    symfunc: only_calculate'+'\n')
        f.write('    print_energy: True'+'\n')
        f.write('    restart_param: FALSE'+'\n')
        f.write('    restart_iter: 0'+'\n')
        f.write('\n'+'\n')
   
        f.write('potential:'+'\n')
        f.write('    potential: ann'+'\n')
