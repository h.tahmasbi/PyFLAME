import io
import os
import sys
import json
import yaml
from collections import defaultdict

from pymatgen.core.structure import Structure
from pymatgen.core.periodic_table import Element

from config import *
from structure.core import min_distance
from structure.perturb import *
from structure.io_db import get_element_list
from flame.core import *

def write_aver_dist_files(structure):
    min_d  = min_distance() 
    energy = 0.01
    forces = False
# write positions and forces for unperturbed structure
    write_p_f_from_Structure(structure, energy, forces, 'position_force_divcheck.yaml')
# write positions and forces for perturbed structures
    perturbed_structures = []
    for attempts in range(0, 200):
        ps = tr_ro(structure,min_d)
        if ps:
            write_p_f_from_Structure(ps, energy, forces, 'position_force_divcheck.yaml')
        ps = ro(structure,min_d)
        if ps:
            write_p_f_from_Structure(ps, energy, forces, 'position_force_divcheck.yaml')
#write SE.ann.input.yaml
    elmnt_list = get_element_list()
    write_SE_ann_FLAME(elmnt_list)    
#write flame_in.yaml
    with open('flame_in.yaml', 'w') as f:
        f.write('main:'+'\n')
        f.write('    task: ann'+'\n')
        f.write('    types:')
        for elmnt in elmnt_list:
            f.write(' {}'.format(elmnt))
        f.write('\n'+'\n')
        f.write('ann:'+'\n')
        f.write('    subtask: check_symmetry_function'+'\n')
        f.write('    etol: 1.E-4'+'\n')
        f.write('    dtol: 25.E-2'+'\n')
        f.write('    normalization: True'+'\n')
        f.write('    read_forces: False'+'\n')
#write list_posinp_check.yaml
    with open('list_posinp_check.yaml', 'w') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_divcheck.yaml'+'\n')

def compute_aver_dist():
    d = defaultdict(list)
    for root, dirs, files in os.walk(os.path.join(Flame_dir,'ave_dis')):
        if 'position_force_divcheck.yaml' in files:
            if 'distall' in files:
                if os.path.getsize(os.path.join(root,'distall')):
                    with open(os.path.join(root,'position_force_divcheck.yaml'), 'r') as f:
                        conf_list = list(yaml.load_all(f, Loader=yaml.FullLoader))
                    nat = conf_list[0]['conf']['nat']
# 
                    dis_list = []
                    with open(os.path.join(root,'distall'), 'r') as f:
                        for line in f:
                            dis_list.append(float(re.split('\s+', line)[4]))
                    d[nat].append(sum(dis_list)/len(dis_list))
                else:
                    with open(log_file, 'a') as f:
                        f.write('WARNING: distall file in {} is empty'.format(root)+'\n')
            else: 
                with open(log_file, 'a') as f:
                    f.write('WARNING: no distall in {}'.format(root)+'\n')
    a_d = {}
    for k in d.keys():
        aa = sum(d[k])/len(d[k])
        a_d[k] = aa
    with open(os.path.join(Flame_dir,'ave_dis','ave_dis.json'), 'w') as f:
        json.dump(a_d, f)

