import os
import subprocess
import shlex

from custodian import Custodian
from fireworks import explicit_serialize, FiretaskBase
from atomate.utils.utils import env_chk, get_logger
from flame.validators import FlameFilesValidator
from flame.handlers import FrozenFlameErrorHandler, FrozenMinhocaoErrorHnadler, FrozenDivcheckErrorHandler 
from flame.jobs import FlameJob
from fireworks.queue.queue_launcher import rapidfire


from fireworks import LaunchPad, FWorker
from fireworks.utilities.fw_serializers import load_object_from_file


logger = get_logger(__name__)


@explicit_serialize
class RunFlameCustodian(FiretaskBase):
    """
    Required params:
        flame_cmd (str): the name of the full executable for running Flame. Supports env_chk.
    Optional params:
        handlers: (str or [ErrorHandler]) - group of handlers to use. See handler_groups_dict dict in the code for
            the groups. Alternatively, you can specify a list of ErrorHandler objects....default is []
        validators: (str or [validatior]) - group of validators to use. See validators_groups_dict dict in the code for
            the groups . Alternatively, you can specify a list of validators  objects....default is [FlameFilesValidator]
    job_type: (str) - choose "minhocao" #TODO This a temporary solution to stop minhocao jobs
    """
    required_params = ["flame_cmd"]
    optional_params = ["handler_group", "validator_group", "job_type"]

    def run_task(self, fw_spec):

        job_type = self.get("job_type", "normal") 
        flame_cmd = env_chk(self["flame_cmd"], fw_spec)
#
        handlers = [FrozenFlameErrorHandler()]
        if job_type == "minhocao":
            handlers = [FrozenMinhocaoErrorHnadler()]
        if job_type == "divcheck":
            handlers = [FrozenDivcheckErrorHandler()]
#
        jobs = [FlameJob(flame_cmd = flame_cmd)]

        c = Custodian(handlers, jobs, validators=[FlameFilesValidator()])
        c.run()

