import io
import os
import sys
import shutil
import re
import time
import json
import yaml
import math
from yaml import Loader 
from itertools import combinations_with_replacement

from pymatgen.core.structure import Structure
from pymatgen.core.periodic_table import Element

from config import *
from structure.core import min_distance, r_cut

def write_p_f_from_yaml(conf,file_name=None): #lattice: 3x3 , sites nx3, forces nx3, energy in Hartree
    cell = conf['conf']['cell']
    coords =  conf['conf']['coord']
    energy = conf['conf']['epot']
    if conf['conf']['force']:
        forces = conf['conf']['force']
    nsite =  conf['conf']['nat']

    with open(file_name, 'a') as f:
        f.write('---'+'\n')
        f.write('conf:'+'\n')
        f.write('  bc: {}'.format(conf['conf']['bc'])+'\n')
        f.write('  cell:'+'\n')
        f.write('  - {}'.format(cell[0])+'\n')
        f.write('  - {}'.format(cell[1])+'\n')
        f.write('  - {}'.format(cell[2])+'\n')
        f.write('  coord:'+'\n')
        for i in range(0,len(coords)):
            f.write('  - {}'.format(coords[i])+'\n')
        f.write('  epot: {}'.format(energy)+'\n')
        if conf['conf']['force']:
            f.write('  force:'+'\n')
            for i in range(0,len(coords)):
                f.write('  - {}'.format(forces[i])+'\n')
        f.write('  nat: {}'.format(nsite)+'\n')
        f.write('  units_length: angstrom'+'\n')

def write_p_f_from_Structure(struct,energy,forces,file_name=None): #lattice: 3x3 , sites nx3, forces nx3, energy in eV
    lattice = struct.lattice.matrix
    sites = struct.sites
    nsite = len(struct.sites)

    with open(file_name, 'a') as f:
        f.write('---'+'\n')
        f.write('conf:'+'\n')
        f.write('  bc: bulk'+'\n')
        f.write('  cell:'+'\n')
        f.write('  - [{:.2f}, {:.2f}, {:.2f}]'.format(lattice[0][0],lattice[0][1],lattice[0][2])+'\n')
        f.write('  - [{:.2f}, {:.2f}, {:.2f}]'.format(lattice[1][0],lattice[1][1],lattice[1][2])+'\n')
        f.write('  - [{:.2f}, {:.2f}, {:.2f}]'.format(lattice[2][0],lattice[2][1],lattice[2][2])+'\n')
        f.write('  coord:'+'\n')
        for i in range(0,len(sites)):
            f.write('  - [{:.9f}, {:.9f}, {:.9f}, {}, TTT]'.format(sites[i].x,sites[i].y,sites[i].z,\
                                         struct.as_dict()['sites'][i]['species'][0]['element'])+'\n')
        f.write('  epot: {}'.format(energy*0.036749309)+'\n') # eV to hartree
        if forces:
            f.write('  force:'+'\n')
            for i in range(0,len(forces)):
                f.write('  - [{}, {}, {}]'.format(forces[i][0]*0.01944689673,forces[i][1]*0.01944689673,forces[i][2]*0.01944689673)+'\n')
        f.write('  nat: {}'.format(nsite)+'\n')
        f.write('  units_length: angstrom'+'\n')

def write_SE_ann_FLAME(elmnt_list):
    g02_list = ['0.0010  0.0000  0.0000  0.0000', '0.0100  0.0000  0.0000  0.0000', '0.0200  0.0000  0.0000  0.0000', '0.0350  0.0000  0.0000  0.0000', '0.0600  0.0000  0.0000  0.0000', '0.1000  0.0000  0.0000  0.0000', '0.2000  0.0000  0.0000  0.0000', '0.4000  0.0000  0.0000  0.0000']
    g05_list = ['0.0001  1.0000  1.0000  0.0000  0.0000', '0.0001  1.0000 -1.0000  0.0000  0.0000', '0.0001  2.0000  1.0000  0.0000  0.0000','0.0001  2.0000 -1.0000  0.0000  0.0000','0.0001  4.0000  1.0000  0.0000  0.0000','0.0001  4.0000 -1.0000  0.0000  0.0000','0.0080  1.0000  1.0000  0.0000  0.0000','0.0080  1.0000 -1.0000  0.0000  0.0000','0.0080  2.0000  1.0000  0.0000  0.0000','0.0080  2.0000 -1.0000  0.0000  0.0000','0.0080  4.0000  1.0000  0.0000  0.0000','0.0080  4.0000 -1.0000  0.0000  0.0000','0.0250  1.0000  1.0000  0.0000  0.0000','0.0250  1.0000 -1.0000  0.0000  0.0000','0.0250  2.0000  1.0000  0.0000  0.0000','0.0250  2.0000 -1.0000  0.0000  0.0000','0.0250  4.0000  1.0000  0.0000  0.0000','0.0250  4.0000 -1.0000  0.0000  0.0000']
    rcut = r_cut() * 1.88973 #A to Bohr
    F_number_of_nodes = input_list['number_of_nodes']
    F_ener_ref = 0.0
    F_method = input_list['method']
    combinations_index = list(combinations_with_replacement(range(len(elmnt_list)),2))
    for elmnt in elmnt_list:
        f_name = str(elmnt) + '.ann.input.yaml'
        with open(f_name, 'a') as f:
            f.write('main:'+'\n')
            f.write('    nodes: [{},{}]'.format(F_number_of_nodes,F_number_of_nodes)+'\n')
            f.write('    rcut:        {}'.format(rcut)+'\n')
            f.write('    ener_ref:    {}'.format(F_ener_ref)+'\n')
            f.write('    method:      {}'.format(F_method)+'\n'+'\n')
            f.write('symfunc:'+'\n')
            n = 0
            for g in g02_list:
                for i in range(0,len(elmnt_list)):
                    n = n + 1
                    f.write('    g02_{:03d}: {}    {}'.format(n,g,elmnt_list[i])+'\n')
            n = 0
            for g in g05_list:
                for i in range(0,len(combinations_index)):
                    n = n + 1
                    f.write('    g05_{:03d}: {}    {}    {}'.format(n,g,elmnt_list[combinations_index[i][0]],\
                                                                        elmnt_list[combinations_index[i][1]])+'\n')
