import os 
import yaml
from pymatgen.core.periodic_table import Specie
from pymatgen.core.structure import Structure
from pymatgen.core.composition import Composition
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.ext.matproj import MPRester
mpr= MPRester()

from config import *

def get_original_species():
    """
    The original_species list will be empty if the oxidation states are not known.
    """
    original_species = []
    elmnt_list = []
#if MP_ID
    if input_list['Composition'][0]['MP_ID']:
        with open(log_file, 'a') as f:
            f.write('The composition is specified by MP-ID: {}'.format(input_list['Composition'][0]['MP_ID'])+'\n')
#
        original_struct = mpr.get_structure_by_material_id(input_list['Composition'][0]['MP_ID'], conventional_unit_cell=False)
        auto_oxi = AutoOxiStateDecorationTransformation()
        original_struct_oxi_struct = False 
        try:
            original_struct_oxi_struct = auto_oxi.apply_transformation(original_struct)
        except:
            with open(log_file, 'a') as f:
                f.write('WARNING: the oxidation states are not known'+'\n')
#
        if original_struct_oxi_struct:
            for spcs in original_struct_oxi_struct.species:
                if spcs not in original_species: 
                    original_species.append(spcs)
                    elmnt_list.append(str(spcs.element))
        else:
            for elmnt in original_struct.species:
                if str(elmnt) not in elmnt_list:
                    elmnt_list.append(str(elmnt))
        return original_species, elmnt_list
#if species
    elif input_list['Composition'][1]['species']:
        with open(log_file, 'a') as f:
            f.write('The composition is specified by species: {}'.format(input_list['Composition'][1]['species'])+'\n')
        for spcs in input_list['Composition'][1]['species']:
            original_species.append(Specie(spcs[0],spcs[1]))
            elmnt_list.append(spcs[0])
        return original_species, elmnt_list
#if chemical_formula
    elif input_list['Composition'][2]['chemical_formula']:
        with open(log_file, 'a') as f:
            f.write('The composition is specified by chemical formula: {}'.format(input_list['Composition'][2]['chemical_formula'])+'\n')
        comp  = Composition(input_list['Composition'][2]['chemical_formula'][0])
        for elmnt in comp.elements:
            elmnt_list.append(elmnt)
        return original_species, elmnt_list
    else:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: no composition is provided. <<<'+'\n')
        exit()

def get_original_composition():
    seen_comp = []
    comps = []
    for frml in input_list['Composition'][2]['chemical_formula']:
        found = False
        for s_c in seen_comp:
            if Composition(frml).reduced_formula == s_c.reduced_formula:
                found = True
                break
        if not found:
            seen_comp.append(Composition(frml))
            comps.append(Composition(frml).reduced_formula)
    return comps

def get_known_structures(original_species):
    original_chemsys = ''
    elmnt_list = []
    for spcs in original_species:
        elmnt_list.append(str(spcs.element))
    original_chemsys= ('-'.join(elmnt_list))
    known_s = mpr.get_structures(original_chemsys)
#
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write('Number of known structures in MPD for the {} system : {}'.format(original_chemsys, len(known_s))+'\n')
#only structures with #atoms larger than 3 and smaller than max_atom_bulk
    known_structs_dict = []
    for struct in known_s:
        if len(struct.sites) < input_list['max_atom_bulk'] and len(struct.sites) > 3:
            known_structs_dict.append(struct.as_dict())

    if len(known_structs_dict) == 0:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed. No bulk structure for the {} system  was found <<<'.format(original_chemsys)+'\n')
            exit()
    else:
        with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
            f.write('Number of known structures in MPD for the {} system with number of atoms between {} and {} :{}'\
                   .format(original_chemsys,input_list['min_atom_bulk'],  input_list['max_atom_bulk'], len(known_structs_dict))+'\n')
#create object to determine oxidation states at each lattice site of known structures
    auto_oxi = AutoOxiStateDecorationTransformation() 
    known_structs_oxi_structs = []
    for struct in known_structs_dict:
        try:
            known_structs_oxi_structs.append(auto_oxi.apply_transformation(Structure.from_dict(struct))) 
        except:
            with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
                f.write('Cannot find the oxidation states for: {}'.format(Structure.from_dict(struct).formula)+'\n')
            continue 
# filter for the same oxidation states 
    final_known_structs_dict = []
    for struct in known_structs_oxi_structs:
        currect_oxi_state = True
        for spcs in struct.species:
            if spcs not in original_species:
                currect_oxi_state = False
                with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
                    f.write('The oxidation states of {} do not match with the original species({})'.format(struct.formula, original_species)+'\n') 
                break
        if currect_oxi_state:
            final_known_structs_dict.append(struct.as_dict()) 
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write('Number of structures with original speceis and same oxidation states: {}'.format(len(final_known_structs_dict))+'\n')
    return final_known_structs_dict


def write_elements(elmnt_list):
        with open(os.path.join(bulk_structure_optimization_dir,'elements.dat'), 'w') as f:
            f.writelines(["%s\n" % el  for el in elmnt_list])

def get_element_list():
    with open(os.path.join(bulk_structure_optimization_dir,'elements.dat'), 'r') as f:
        elmnt_list = [line.strip() for line in f]
    return elmnt_list 

