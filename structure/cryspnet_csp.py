import pandas as pd
from collections import defaultdict

import os 
import yaml
import json
from pymatgen.core.composition import Composition
from structure.random_structures import generate_random_bulk_space_group
from config import *

def get_space_groups(comps, t_b, t_sg, bravais_prob_cutoff, spacegroup_prob_cutoff):
    from cryspnet.models import load_Bravais_models, load_Lattice_models, load_SpaceGroup_models
    from cryspnet.utils import FeatureGenerator, topkacc
#
    formulas = []
    for a_comp in comps:
       formulas.append(Composition(a_comp).reduced_formula)

    BE  = load_Bravais_models()
    LPB = load_Lattice_models()
    SGB = load_SpaceGroup_models()
# 
    spacegroups_dict = defaultdict(list)
    for formula in formulas:
        pd_formula = pd.DataFrame({'formula':[formula]})
        featurizer = FeatureGenerator()
        predictors = featurizer.generate(pd_formula)
#
        bravais_probs, bravais = BE.predicts(predictors, topn_bravais = t_b)
#
        bravais_list = []
        for i in range(t_b):
            if bravais_probs[:, i] >= bravais_prob_cutoff:
                bravais_list.append(bravais[:, i])
#
        spacegroup_list = []
        for b in bravais_list:
            predictors['Bravais'] = b

            sg_prob, sg = SGB.predicts(predictors, topn_spacegroup = t_sg)
            for i in range(t_sg):
                if sg_prob[:,i] > spacegroup_prob_cutoff:
                    spacegroup_list.append(int(sg[:,i].tolist()[0]))
        spacegroups_dict[str(formula)].extend(spacegroup_list)
    return spacegroups_dict

def cryspnet_csp(formulas, space_groups):
    cryspnet_structures = []
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write('CRYSPNet crystal structure prediction for {}'.format(formulas)+'\n')

    for i in range(len(formulas)):
        with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
            f.write('chemical formula: {}, space groups: {}'.format(formulas[i], space_groups[i])+'\n')
        elements = []
        n_elmnt = []
        for e, n in Composition(formulas[i]).items():
            elements.append(str(e))
            n_elmnt.append(int(n))
        for a_sg in space_groups[i]:
            r_b = generate_random_bulk_space_group(a_sg, elements, n_elmnt, 1.1, min_n_atom = input_list['min_atom_bulk'], max_n_atom = input_list['max_atom_bulk'])
            cryspnet_structures.extend(r_b)
            with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
                f.write('Number of bulk structures predicted by Crystal Structure Prediction Network (CRYSPNet) with space group {}: {}'.format(int(a_sg), len(r_b))+'\n')

# store structures
    if len(cryspnet_structures) == 0:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed. No bulk structure for optimixation <<<'+'\n')
            exit()
    else:
        cryspnet_structures_dict = []
        for i in range(len(cryspnet_structures)):
            cryspnet_structures_dict.append(cryspnet_structures[i].as_dict())
        with open(os.path.join(bulk_structure_optimization_dir,'all_bulk_structures.json'),'w') as f:
            json.dump(cryspnet_structures_dict, f)
        with open(log_file, 'a') as f:
             f.write('Number of bulk structures predicted by Crystal Structure Prediction Network (CRYSPNet): {}'.format(len(cryspnet_structures))+'\n')

