import io
import os
import sys
from sys import exit
from datetime import datetime
import json 
import yaml
import numpy as np
from random import random, randint, uniform, sample
from pymatgen.core.structure import Structure
from pymatgen.core.periodic_table import Element, Specie
from pymatgen.analysis.molecule_structure_comparator import CovalentRadius

from config import *
from structure.core import min_distance

### random site rotation
def ro(struct, min_d):
    i_random_1 = randint(0, len(struct.sites)-1)
    i_random_2 = randint(0, len(struct.sites)-1)
    while (i_random_2==i_random_1):
        i_random_2 = randint(0, len(struct.sites)-1)
    for attempts in range(0,200):
        structure = False
        t = uniform(10,180)
        a_random = uniform(0,struct.lattice.a)
        b_random = uniform(0,struct.lattice.b)
        c_random = uniform(0,struct.lattice.c)

        struct.rotate_sites(indices=[i_random_1], theta=t, axis=[random(),random(),random()], anchor=[a_random,b_random,c_random], to_unit_cell=True)
        struct.rotate_sites(indices=[i_random_2], theta=t, axis=[random(),random(),random()], anchor=[a_random,b_random,c_random], to_unit_cell=True)
#Create a structure from the previous one with validate_proximity=True
        crdnts = []
        spcs = []
        for coord in struct:
            crdnts.append(coord.coords)
            spcs.append(coord.species)
        try:
          structure = Structure(struct.lattice, spcs, crdnts, coords_are_cartesian=True, validate_proximity=True)
        except:
            structure = False
        if structure:
            if(structure.is_valid(min_d)):
                return structure
# less perturbed structures
    i_random_1 = randint(0, len(struct.sites)-1)
    for attempts in range(0,200):
        structure = False
        t = uniform(10,180)
        a_random = uniform(0,struct.lattice.a)
        b_random = uniform(0,struct.lattice.b)
        c_random = uniform(0,struct.lattice.c)

        struct.rotate_sites(indices=[i_random_1], theta=t, axis=[random(),random(),random()], anchor=[a_random,b_random,c_random], to_unit_cell=True)
#Create a structure from the previous one with validate_proximity=True
        crdnts = []
        spcs = []
        for coord in struct:
            crdnts.append(coord.coords)
            spcs.append(coord.species)
        try:
          structure = Structure(struct.lattice, spcs, crdnts, coords_are_cartesian=True, validate_proximity=True)
        except:
            structure = False
        if structure:
            if(structure.is_valid(min_d)):
                return structure
    return False 

#### random translate + rotation 
def tr_ro(struct, min_d):
    i_random_1 = randint(0, len(struct.sites)-1)
    i_random_2 = randint(0, len(struct.sites)-1)
    for attempts in range(0,200):
        structure = False
        t = uniform(10,180)
        a_random = uniform(0,struct.lattice.a)
        b_random = uniform(0,struct.lattice.b)
        c_random = uniform(0,struct.lattice.c)

        struct.translate_sites(indices=[i_random_1], vector=[a_random,b_random,c_random], to_unit_cell=True)
        struct.rotate_sites(indices = [i_random_2], theta=t, axis=[random(),random(),random()], anchor=[a_random,b_random,c_random], to_unit_cell=True)
#Create a structure from the previous one with validate_proximity=True
        crdnts = []
        spcs = []
        for coord in struct:
            crdnts.append(coord.coords)
            spcs.append(coord.species)
        try:
          structure = Structure(struct.lattice, spcs, crdnts, coords_are_cartesian=True, validate_proximity=True)
        except:
            structure = False
        if structure:
            if(structure.is_valid(min_d)):
                return structure
#less perturbed structures
    i_random_1 = randint(0, len(struct.sites)-1)
    for attempts in range(0,200):
        structure = False
        t = uniform(10,180)
        a_random = uniform(0,struct.lattice.a)
        b_random = uniform(0,struct.lattice.b)
        c_random = uniform(0,struct.lattice.c)

        struct.translate_sites(indices=[i_random_1], vector=[a_random,b_random,c_random], to_unit_cell=True)
        struct.rotate_sites(indices = [i_random_1], theta=t, axis=[random(),random(),random()], anchor=[a_random,b_random,c_random], to_unit_cell=True)
#Create a structure from the previous one with validate_proximity=True
        crdnts = []
        spcs = []
        for coord in struct:
            crdnts.append(coord.coords)
            spcs.append(coord.species)
        try:
          structure = Structure(struct.lattice, spcs, crdnts, coords_are_cartesian=True, validate_proximity=True)
        except:
            structure = False
        if structure:
            if(structure.is_valid(min_d)):
                return structure
    return False

#### compressed_structures
def compress(struct):
    min_d = min_distance()*0.80
    comp_structs = []
    for cv in np.arange(0.82,0.98,0.03):
        vn = struct.volume*cv
        struct.scale_lattice(vn)
        crdnts = []
        spcs = []
        for coord in struct:
            crdnts.append(coord.coords)
            spcs.append(coord.species)
        try:
            structure = Structure(struct.lattice, spcs, crdnts, coords_are_cartesian=True, validate_proximity=True)
        except:
            structure = False
        if structure:
            if(structure.is_valid(min_d)):
                comp_structs.append(Structure.as_dict(struct))
    return comp_structs 

#### expanded_structures
def expand(struct):
    expa_structs = []
    for cv in np.arange(1.00,1.22,0.03):
        vn = struct.volume*cv
        struct.scale_lattice(vn)
        expa_structs.append(Structure.as_dict(struct))
    return expa_structs 

### create perturbed structures ###
def create_perturbed_structures():
    with open(os.path.join(bulk_structure_optimization_dir,'final_relaxed_bulk_structures.json'), 'r') as f:
        bulk_relaxed_structures=json.loads(f.read())

    with open(os.path.join(perturbed_bulk_structure_optimization_dir,'perturbed-bulk_stressed-structure.dat'), 'a') as f:
        f.write("Number of relaxed bulk structures with number of atom between {} and {}: {}".format(4, input_list['max_atom_bulk'] ,len(bulk_relaxed_structures))+'\n')
# min_d
    min_d = min_distance()
    with open(os.path.join(perturbed_bulk_structure_optimization_dir,'perturbed-bulk_stressed-structure.dat'), 'a') as f:
        f.write("Accepted minimum distance between atoms in the perturbed structre: {}".format(min_d)+'\n')
# loop for creating structures
    for struct_num, s in enumerate(bulk_relaxed_structures):
        struct = Structure.from_dict(s)
# number of attemts for perturbed structures 
        n_attempt = round(0.5 * input_list['max_number_of_perturbed_structures']/len(bulk_relaxed_structures))
#
        rotated_structures_dict = []
        tr_ro_structures_dict = []
        stressed_structures_dict = []
#
        for j in range(0, n_attempt):
            structure = ro(struct, min_d)
            if structure:
                rotated_structures_dict.append(structure.as_dict())
            structure = tr_ro(struct, min_d)
            if structure:
                tr_ro_structures_dict.append(structure.as_dict())
# stressed structures
        stressed_structures_dict.extend(compress(struct))
        stressed_structures_dict.extend(expand(struct))

# store perturbed structures
        perturbed_bulk_structures_dict = rotated_structures_dict + tr_ro_structures_dict + stressed_structures_dict 
        f_name = os.path.join(perturbed_bulk_structure_optimization_dir,'struct-'+ str(struct_num+1)+ '-structures.json')
        with open(f_name, 'w') as f:
            json.dump(perturbed_bulk_structures_dict, f)

        with open(os.path.join(perturbed_bulk_structure_optimization_dir,'perturbed-bulk_stressed-structure.dat'), 'a') as f:
            f.write('structure {} ({} atoms):'.format(str(struct_num+1),len(Structure.from_dict(s).sites))+'\n')
            f.write('{} rotated and {} translated-rotated structures; {} stressed structures => {} bulk structures'\
                   .format(len(rotated_structures_dict),len(tr_ro_structures_dict),len(stressed_structures_dict),\
                           len(perturbed_bulk_structures_dict))+'\n')

