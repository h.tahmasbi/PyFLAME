import os
import sys
from sys import exit

from config import *
from cryspnet.utils import FeatureGenerator, topkacc
 
__author__ = "Hossein Mirhosseini"
__copyright__ = ""
__maintainer__ = "Hossein Mirhosseini"
__email__ = "mirhosse@mail.uni-paderborn.de"

def main():
    stpnmbr = restart['re-start']
    if stpnmbr == 1:
        from workflows.step1 import step_1
        if step_1():
            stpnmbr = 2
    if stpnmbr == 2:
        from workflows.step2 import step_2
        if step_2():
            stpnmbr = 3
    if stpnmbr == 3:
        from workflows.step3 import step_3
        if step_3():
            stpnmbr = 4
    if stpnmbr == 4:
        from workflows.step4 import step_4
        if step_4():
            stpnmbr = 5
    if stpnmbr == 5:
        from workflows.step5 import step_5
        if step_5():
            stpnmbr = 6
    if stpnmbr == 6:
        from workflows.step6 import step_6
        if step_6():
            stpnmbr = 7
    if stpnmbr == 7:
        from workflows.step7 import step_7
        step_7()
        exit()
    if stpnmbr == 0:
        from workflows.core import rerun
        rerun()
        exit()
    

if __name__ == "__main__":
    main()
