import os
import sys
import json
from datetime import datetime
from sys import exit

from config import *
from workflows.core import *
from vasp.workflows import get_vasp_wf
def step_5():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 5'+'\n')
        f.write('Start time: {}'.format(datetime.now())+'\n')
#LPAD reset
    if run_exists():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
        exit()
#add wfs to LPAD
    wfname_list = []
    for files in os.listdir(perturbed_bulk_structure_optimization_dir):
        fname, fextension = os.path.splitext(files)
        if fextension == '.json':
            with open(log_file, 'a') as f:
                f.write('Adding structures from {}'.format(os.path.join(perturbed_bulk_structure_optimization_dir,files))+'\n')
            with open(os.path.join(perturbed_bulk_structure_optimization_dir,files), 'r') as f:
                perturbed_s = json.loads(f.read())
# get ws
            vasp_perturbed_wf = get_vasp_wf(perturbed_s, 'perturbed-bulk', wf_name = 'perturbed-struct-'+fname.split('-')[1])
# add wf
            add_wf(vasp_perturbed_wf)
            wfname_list.append('perturbed-struct-'+fname.split('-')[1])
# additional structures
    if input_list['additional_structures']:
        for files in os.listdir(additional_structures_dir):
            fname, fextension = os.path.splitext(files)
            if fextension == '.json':
                with open(log_file, 'a') as f:
                    f.write('Adding structures from {}'.format(os.path.join(additional_structures_dir,files))+'\n')
                with open(os.path.join(additional_structures_dir,files), 'r') as f:
                    additional_s = json.loads(f.read())
# get ws
                vasp_additional_wf = get_vasp_wf(additional_s, 'perturbed-bulk', wf_name = 'additional-structs')
# add wf
                add_wf(vasp_additional_wf)
                wfname_list.append('additional-structs')
# run jobs
    if not run_jobs('perturbed-bulk', perturbed_bulk_structure_optimization_dir):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed <<<'+'\n')
        exit()
# wait until all jobs are done
    while True:
        if run_exists():
            fizzle_lostruns()
            sleep(60)
        else:
           break
# check launchpad state
    lp_state = check_lp(wfname_list)
    if lp_state == 'FIZZLED':
        with open(log_file, 'a') as f:
            f.write('WARNING: bulk workflow is FIZZLED'+'\n')
    if lp_state == 'unknown':
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
        exit()
#
    with open(log_file, 'a') as f:
        f.write('STEP 5 ended.'+'\n')
        f.write('End time: {}'.format(datetime.now())+'\n')
    return steps_status[5] 

