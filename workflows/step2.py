import os
import sys
import json
from time import sleep
from datetime import datetime
from sys import exit
from config import *
from workflows.core import *
from vasp.workflows import *

def step_2():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 2'+'\n')
        f.write('Start time: {}'.format(datetime.now())+'\n')
# check the lpad LPAD
    if run_exists():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
        exit()
# read structures
    if input_list['fixed_stoichiometry']:
        with open(log_file, 'a') as f:
           f.write('Adding bulk structures with fixed stoichiometry from {}'.format(os.path.join(bulk_structure_optimization_dir,'fixed_stoichiometry_bulk_structures.json'))+'\n')
        with open(os.path.join(bulk_structure_optimization_dir,'fixed_stoichiometry_bulk_structures.json'), 'r') as f:
            all_s = json.loads(f.read())
    else:   
        with open(log_file, 'a') as f:
            f.write('Adding bulk structures from {}'.format(os.path.join(bulk_structure_optimization_dir,'all_bulk_structures.json'))+'\n')
        with open(os.path.join(bulk_structure_optimization_dir,'all_bulk_structures.json'), 'r') as f:
            all_s = json.loads(f.read())
# get workflow
    bulk_wf = get_vasp_wf(all_s, 'bulk', wf_name = 'bulk-opt')
# add workflow
    add_wf(bulk_wf)
# run jobs
    if not run_jobs('bulk', bulk_structure_optimization_dir):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed <<<'+'\n')
        exit()
# wait until all jobs are done
    while True:
        if run_exists():
            fizzle_lostruns()
            sleep(60)
        else:
           break 
# check launchpad state
    lp_state = check_lp(['bulk-opt']) 
    if lp_state == 'FIZZLED':
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: bulk workflow is FIZZLED <<<'+'\n')
    if lp_state == 'unknown':
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
        exit()
#
    with open(log_file, 'a') as f:
        f.write('STEP 2 ended.'+'\n')
        f.write('End time: {}'.format(datetime.now())+'\n')
    return steps_status[2] 

