import os
import sys
import yaml
from sys import exit
from datetime import datetime 

from cryspnet.utils import FeatureGenerator, topkacc
from cryspnet.config import * 

from config import *
from structure.io_db import *
from workflows.core import *
from structure.workflows import *  
from structure.cryspnet_csp import get_space_groups

def step_1():
    with open(log_file, 'a') as f:
        f.write('STEP 1'+'\n')
        f.write('Start time: {}'.format(datetime.now())+'\n')
        f.write('For more details see {}/bulk_structure.dat'.format(bulk_structure_optimization_dir)+'\n')
# read the original composition
    original_species = []
    elment_list = []
    composition = []
    original_species, element_list = get_original_species()
    if input_list['Composition'][2]['chemical_formula']:   
        compositions = get_original_composition()
# create folders
    try:
        os.mkdir(bulk_structure_optimization_dir)
    except FileExistsError:
        with open(log_file, 'a') as f:
            f.write(">>> Cannot proceed. {} exists. <<".format(bulk_structure_optimization_dir)+'\n')
        exit()
# write elements in elements.dat 
    write_elements(element_list)
# reset LPAD
    if run_exists():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
        exit()
    elif not launchpad_reset():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: cannot reset Launchpad <<<'+'\n')
        exit()
    else:
        with open(log_file, 'a') as f:
                f.write('Reseting Launchpad'+'\n')
# list of wf to be added to lp
    wfname_list = []
# Ionic-subctitution crystal scructure prediction
    if input_list['crystal_structure_prediction_method'] == 1:
        if len(original_species) > 0: 
            with open(log_file, 'a') as f:
                f.write('Ionic-subctitution crystal structure prediction'+'\n')
# get i_s_csp_wf
            i_s_csp_wf = get_i_s_csp_wf(original_species)
# add to lp
            add_wf(i_s_csp_wf)
            wfname_list.append('csp')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: it is not possible to perform ionic-subctitution crystal structure prediction. \
                             Oxidations states are not known. <<<'+'\n')
            exit()
# CRYSPNet crystal structure prediction
    elif input_list['crystal_structure_prediction_method'] == 2:
        if len(compositions) > 0:
            with open(log_file, 'a') as f:
                f.write('CRYSPNet crystal structure prediction'+'\n')
#
            sg_dict = get_space_groups(compositions, 14, 50, bravais_prob_cutoff = input_list['threshold'], spacegroup_prob_cutoff = input_list['threshold'])
            formulas = []
            space_groups = []
            for frml, sgs in sg_dict.items():
                formulas.append(frml)
                space_groups.append(sgs)
# get cryspnet_csp_wf
            cryspnet_csp_wf = get_cryspnet_csp_wf(formulas, space_groups)
# add to lp
            add_wf(cryspnet_csp_wf)
            wfname_list.append('csp')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: it is not possible to perform CRYSPNet crystal structure prediction. \
                             No chemical formula is provided. <<<'+'\n')
            exit()
# random structure generation
    elif input_list['crystal_structure_prediction_method'] == 0:
        if len(compositions) > 0:
            with open(log_file, 'a') as f:
                f.write('Random bulk structures generattion by PyXtal'+'\n')
# get random_struct_wf
            random_struct_wf = get_random_struct_wf(compositions, 3)
# add to lp
            add_wf(random_struct_wf)
            wfname_list.append('random_bulk')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: it is not possible to perform PyXtal random structure generation. \
                             No chemical formula is provided. <<<'+'\n')
            exit()
    else:
        with open(log_file, 'a') as f:
            f.write('WARNING: neither crystal structure prediction nor random structure generation is performed'+'\n')
# run csp/random structure generation jobs
    if not run_jobs('csp', bulk_structure_optimization_dir):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed <<<'+'\n')
        exit()
# wait until all jobs are done
    while True:
        if run_exists():
            fizzle_lostruns()
            sleep(60)
        else:
           break
# clusters
# list of wf to be added to lp
    if input_list['cluster_calculation']:
        with open(log_file, 'a') as f:
            f.write('Random cluster generation by PyXtal'+'\n')
# get random_cluster_wf
        random_cluster_wf = get_random_struct_wf(compositions, 0)
# add to lp
        add_wf(random_cluster_wf)
        wfname_list.append('random_cluster')
# run csp/random structure generation jobs
    if not run_jobs('csp', bulk_structure_optimization_dir):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed <<<'+'\n')
        exit()
# wait until all jobs are done
    while True:
        if run_exists():
            fizzle_lostruns()
            sleep(60)
        else:
           break
# check launchpad state
    lp_state = check_lp(wfname_list)
    if lp_state == 'FIZZLED':
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: csp/random structure workflow is FIZZLED <<<'+'\n')
    if lp_state == 'unknown':
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
        exit()
# user ion substitution
    if input_list['ionic_substitution']:
        with open(log_file, 'a') as f:
            f.write('User ion substitution'+'\n')
        from structure.user_ion_substitution import user_ion_substitution
        user_ion_substitution()
# 
    with open(log_file, 'a') as f:
        f.write('STEP 1 ended'+'\n')
        f.write('End time: {}'.format(datetime.now())+'\n')
    return steps_status[1]
