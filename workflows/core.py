import os
import yaml
from time import sleep

from config import *
from fireworks import LaunchPad, FWorker
from fireworks.queue.queue_launcher import rapidfire
from fireworks.utilities.fw_serializers import load_object_from_file

def launchpad_reset():
    with open(os.path.join(CONFIG_DIR,'my_launchpad.yaml'),'r') as file:
        lpd = yaml.full_load(file)
    file.close()

    l_host = lpd['host']
    l_port = lpd['port']
    l_name = lpd['name']
    l_username = lpd['username']
    l_password = lpd['password']

    launchpad = LaunchPad(host = l_host, port = l_port, name = l_name, username = l_username, password = l_password, ssl = True, authsource = 'admin')
    try:
        launchpad.reset('', require_password = False, max_reset_wo_password = 10000)
        return True
    except:
        return False


def add_wf(wf):
    with open(os.path.join(CONFIG_DIR,'my_launchpad.yaml'),'r') as file:
        lpd = yaml.full_load(file)
    file.close()

    l_host = lpd['host']
    l_port = lpd['port']
    l_name = lpd['name']
    l_username = lpd['username']
    l_password = lpd['password']

    launchpad = LaunchPad(host = l_host, port = l_port, name = l_name, username = l_username, password = l_password, ssl = True, authsource = 'admin')
    launchpad.add_wf(wf)

def run_jobs(job_type, l_dir):
    with open('./workflows/job_script/job_script.yaml', 'r') as f:
        js = yaml.load(f, Loader=yaml.FullLoader)

    with open(os.path.join(CONFIG_DIR,'my_launchpad.yaml'),'r') as file:
        lpd = yaml.full_load(file)
    l_host = lpd['host']
    l_port = lpd['port']
    l_name = lpd['name']
    l_username = lpd['username']
    l_password = lpd['password']

    launchpad = LaunchPad(host = l_host, port = l_port, name = l_name, username = l_username, password = l_password, ssl = True, authsource = 'admin')

#set up qadapter
    fworker =  FWorker.from_file(os.path.join(CONFIG_DIR,'my_fworker.yaml'))
    qadapter = load_object_from_file(os.path.join(CONFIG_DIR,'my_qadapter.yaml'))
    with open('./workflows/job_script/list_of_jobtypes.yaml', 'r') as f:
        ljts = yaml.load(f, Loader=yaml.FullLoader)
    job_type_list = ljts['job_type_list']
    if job_type in job_type_list:
        nqueue = js[job_type]['number_of_jobs']
        if js[job_type]['number_of_node']:
            qadapter['nodes'] = js[job_type]['number_of_node']
        if js[job_type]['number_of_cpu']:
            qadapter['ncpu']  = js[job_type]['number_of_cpu']
        if js[job_type]['time']:
            qadapter['time']  = js[job_type]['time']
    else:
        return False
#run in rapidfire mode 
    rapidfire(launchpad, fworker, qadapter, launch_dir=l_dir, njobs_queue=nqueue, nlaunches=0, reserve=False)
    return True

def run_exists():
    state = False
    with open(os.path.join(CONFIG_DIR,'my_launchpad.yaml'),'r') as file:
        lpd = yaml.full_load(file)
    l_host = lpd['host']
    l_port = lpd['port']
    l_name = lpd['name']
    l_username = lpd['username']
    l_password = lpd['password']
    launchpad = LaunchPad(host = l_host, port = l_port, name = l_name, username = l_username, password = l_password, ssl = True, authsource = 'admin')
#
    if launchpad.future_run_exists() or launchpad.get_fw_ids(query={'state': 'RUNNING'}):
        state = True
    return state

def check_lp(wfname_list):
    with open(os.path.join(CONFIG_DIR,'my_launchpad.yaml'),'r') as file:
        lpd = yaml.full_load(file)
    l_host = lpd['host']
    l_port = lpd['port']
    l_name = lpd['name']
    l_username = lpd['username']
    l_password = lpd['password']
    launchpad = LaunchPad(host = l_host, port = l_port, name = l_name, username = l_username, password = l_password, ssl = True, authsource = 'admin')
#
    for wfname in wfname_list:
        try:
            wfid = launchpad.get_wf_ids({"name": wfname})
            wf_summary = launchpad.get_wf_summary_dict(wfid[0],mode="less")
        except:
            return 'unknown'
        if wf_summary['state'] == 'FIZZLED':
             return wf_summary['state']
    return wf_summary['state']

def fizzle_lostruns():
    with open(os.path.join(CONFIG_DIR,'my_launchpad.yaml'),'r') as file:
        lpd = yaml.full_load(file)
    l_host = lpd['host']
    l_port = lpd['port']
    l_name = lpd['name']
    l_username = lpd['username']
    l_password = lpd['password']
    launchpad = LaunchPad(host = l_host, port = l_port, name = l_name, username = l_username, password = l_password, ssl = True, authsource = 'admin')
    launchpad.detect_lostruns(expiration_secs=10800, fizzle=True)

def rerun():
    with open(os.path.join(CONFIG_DIR,'my_launchpad.yaml'),'r') as file:
        lpd = yaml.full_load(file)
    l_host = lpd['host']
    l_port = lpd['port']
    l_name = lpd['name']
    l_username = lpd['username']
    l_password = lpd['password']
    launchpad = LaunchPad(host = l_host, port = l_port, name = l_name, username = l_username, password = l_password, ssl = True, authsource = 'admin')
    fwids = launchpad.get_fw_ids(query={"state":"READY"})
    if fwids:
        wf = launchpad.get_wf_by_fw_id(fwids[0])
        wf_name = wf.name
        stpnm = wf_name.split('_')[1]
        stpnmbr = wf_name.split('_')[-1]
        run_jobs(stpnm, os.path.join(Flame_dir,stpnmbr,stpnm))
